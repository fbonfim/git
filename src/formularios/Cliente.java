/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

/**
 *
 * @author Felipe Matheus
 */
public class Cliente {
    
    private int id;
    private String descricao;
    
    public Cliente(){
        
    }
    public Cliente (int id, String descricao){
        
        this.id = id;
        this.descricao = descricao;
    }
    
    public int getIDCliente(){
        return id;
    }
    
    public void setIdCliente(int id){
        this.id = id;
    }
    
        public String getDesCliente(){
        return descricao;
    }
    
    public void setDesCliente(String descricao){
        this.descricao = descricao;
    }
    public String toString(){
        
        return descricao;
    }
}
