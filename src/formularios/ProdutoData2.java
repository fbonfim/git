/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Felipe Matheus
 */
public class ProdutoData2 extends conectar{
    
    public ProdutoData2() throws Exception{}
    public Vector<Cliente> listarClientes()  throws Exception{
        conectar cc6 = new conectar();
        Connection cn6 = cc6.conexion();
        String url = "SELECT * FROM clientes";
        Vector<Cliente> client = new Vector<Cliente>();
        PreparedStatement ps6 =  cn6.prepareStatement(url);
        ResultSet rs2 = ps6.executeQuery();
        while(rs2.next()){
            Cliente obj2 = new Cliente();
            obj2.setDesCliente(rs2.getString("nome_cliente"));
            client.add(obj2);
        }
        return client;
                }
    
}
